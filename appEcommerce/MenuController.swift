//
//  MenuController.swift
//  SidebarMenu
//
//  Created by Simon Ng on 2/2/15.
//  Copyright (c) 2015 AppCoda. All rights reserved.
//

import UIKit

class MenuController: UITableViewController {
  
    var categorias:[String] = []
    var menuLength:Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //myTableView.delegate = self
        //myTableView.dataSource = self
        
        for cat:CategoriaRow in GlobalVariables.Categorias
        {
            categorias.append(cat.Principal.Name)
        }
        
        self.menuLength = GlobalVariables.Categorias.count + 19
        
        // Uncomment the following line to preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        //self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //CODE TO BE RUN ON CELL TOUCH
        
        if((indexPath.row > 1)&&(indexPath.row < GlobalVariables.Categorias.count+2))
        {
            GlobalVariables.mainCategorie = indexPath.row - 2
            GlobalVariables.subCategorie = 0
        }
    }
    
    /* `UITableView` consists of sections and rows
    default is a single section table, i.e., a plain list
    method returns the number of rows in this default section */
    override func tableView(tableView: UITableView,
        numberOfRowsInSection section: Int)
        -> Int {
            return self.menuLength
    }
    
    /* `UITableViewCell` represent individual table cells
    table cells are reused
    a unique identifier is used to group reusable cells
    returns a cell for a given section and row */
    override func tableView(tableView: UITableView,
        cellForRowAtIndexPath indexPath: NSIndexPath)
        -> UITableViewCell {
            
            let menuValue:String!
            let catLength:Int = GlobalVariables.Categorias.count
            //let cell:UITableViewCell!
            
            if((indexPath.row > 1)&&(indexPath.row<catLength+2))
            {
                menuValue = "CategoriaCell"
                let item = self.categorias[indexPath.row-2]
                
                let cell2 = tableView.dequeueReusableCellWithIdentifier("CategoriaCell", forIndexPath: indexPath) as! MenuCustomCell
                //cell.catContent.catLabel?.text = item
                
                cell2.setMenu(item)
                
                return cell2
            }
            else
            {
                switch indexPath.row{
                case 0:
                    menuValue = "Cabecera"
                case 1:
                    menuValue = "Categorias"
                case catLength+2:
                    menuValue = "Cuenta"
                case catLength+3:
                    menuValue = "MisDatos"
                case catLength+4:
                    menuValue = "MisDirecciones"
                case catLength+5:
                    menuValue = "MisPedidos"
                case catLength+6:
                    menuValue = "Favoritos"
                case catLength+7:
                    menuValue = "Idioma"
                case catLength+8:
                    menuValue = "Kukimba"
                case catLength+9:
                    menuValue = "Contacto"
                case catLength+10:
                    menuValue = "AvisoLegal"
                case catLength+11:
                    menuValue = "PoliticaPrivacidad"
                case catLength+12:
                    menuValue = "QuienesSomos"
                case catLength+13:
                    menuValue = "PreguntasFrecuentes"
                case catLength+14:
                    menuValue = "VentajasKukimba"
                case catLength+15:
                    menuValue = "CalidadFiabilidad"
                case catLength+16:
                    menuValue = "CuidadoCalzado"
                case catLength+17:
                    menuValue = "GastosEnvio"
                case catLength+18:
                    menuValue = "GarantiaSatisfaccion"                    
                default:
                    menuValue = "MisDatos"
                }
                let cell = tableView.dequeueReusableCellWithIdentifier(menuValue, forIndexPath: indexPath) as! UITableViewCell
                
                return cell
            }
            
            
    }
}

