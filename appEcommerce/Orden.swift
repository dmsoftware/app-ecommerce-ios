//
//  Orden.swift
//  Kukimba
//
//  Created by imac on 7/7/15.
//  Copyright (c) 2015 Acc. All rights reserved.
//

import Foundation

/**
Data entity that represents the current weather of a Place.
Subclasses NSObject to enable Obj-C instantiation.
*/
class Orden : NSObject
{
    let
    ID: String,
    Name: String
    
    init(id: String, name: String)
    {
        self.ID = id
        self.Name = name
    }
}