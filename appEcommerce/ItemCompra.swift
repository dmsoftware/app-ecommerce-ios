//
//  ItemCompra.swift
//  Kukimba
//
//  Created by imac on 8/7/15.
//  Copyright (c) 2015 Acc. All rights reserved.
//

import Foundation

class ItemCompra : NSObject
{
    let
    Availability: Int,
    Id: Int,
    Image: String,
    ItemBrand: String,
    ItemId: Int,
    ItemName: String,
    OriginalPrice: Float,
    Price: Float,
    SizeDisplay: String,
    SizeId: String,
    Stock: Int,
    Units: Int
    
    init(
        availability: Int,
        id: Int,
        image: String,
        itemBrand: String,
        itemId: Int,
        itemName: String,
        originalPrice: Float,
        price: Float,
        sizeDisplay: String,
        sizeId: String,
        stock: Int,
        units: Int)
    {
        
        self.Availability = availability
        self.Id = id
        self.Image = image
        self.ItemBrand = itemBrand
        self.ItemId = itemId
        self.ItemName = itemName
        self.OriginalPrice = originalPrice
        self.Price = price
        self.SizeDisplay = sizeDisplay
        self.SizeId = sizeId
        self.Stock = stock
        self.Units = units
    }
}