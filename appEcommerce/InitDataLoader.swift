//
//  InitDataLoader.swift
//  Kukimba
//
//  Created by imac on 6/7/15.
//  Copyright (c) 2015 Acc. All rights reserved.
//

import UIKit
//import Alamofire
//import SwiftyJSON

class InitDataLoader: BaseViewController
{
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        GlobalVariables.systemVersion = NSString(string: UIDevice.currentDevice().systemVersion).doubleValue
        
        // La primera petición al servicio web
        
        //fetchCategorias()
    }
    
    private func fetchCategorias()
    {
        var postEndpoint: String = " -- url del servicio -- "
        
        let url = NSURL(string: postEndpoint)
        
        //let task = NSURLSession.sharedSession().dataTaskWithURL(url!) {(data, response, error) in
        if let data = NSData(contentsOfURL: url!, options: .allZeros, error: nil) {
            //println(NSString(data: data, encoding: NSUTF8StringEncoding))
            let post = JSON(data: data)
            // now we have the results, let's just print them though a tableview would definitely be better UI:
            //println(post)
            for index in 0..<post.count-1{
                    
                if(post[index]["Parent"].int == -1)
                {
                    let cat:Categoria = Categoria(id: post[index]["ID"].int!, merchantid: post[index]["MerchantID"].string!, name: post[index]["Name"].string!, parent: post[index]["Parent"].int!,position: post[index]["Position"].int!)
                    
                    let catRow:CategoriaRow = CategoriaRow(principal: cat)
                    
                    GlobalVariables.Categorias.append(catRow)
                }
                else
                {
                    let cat:Categoria = Categoria(id: post[index]["ID"].int!, merchantid: post[index]["MerchantID"].string!, name: post[index]["Name"].string!, parent: post[index]["Parent"].int!,position: post[index]["Position"].int!)
                    
                    let catRow:CategoriaRow = GlobalVariables.Categorias[post[index]["Parent"].int! - 1]
                    catRow.addCat(cat)
                }
            }
        }
        
        self.fetchOrden()
        //task.resume()
    }
    
    private func fetchOrden()
    {
        var postEndpoint: String = "http://www.kukimba.com/_app-api/service-api.aspx?method=GetOrderOptions"
        
        let url = NSURL(string: postEndpoint)
        
        if let data = NSData(contentsOfURL: url!, options: .allZeros, error: nil) {
            //println(NSString(data: data, encoding: NSUTF8StringEncoding))
            let post = JSON(data: data)
            //println(post)
            for index in 0..<post.count-1
            {
                let ord:Orden = Orden(id: post[index]["ID"].string!, name: post[index]["Name"].string!)                        
                GlobalVariables.Ordenes.append(ord)
            }
        }
        //self.fetchCesta()
        self.performSegueWithIdentifier("DataLoaded", sender: nil)
    }
    
    private func fetchCesta(cartId:String, userId:String)
    {
        var postEndpoint: String = "http://www.kukimba.com/_app-api/service-api.aspx?method=GetShoppingCart&shoppingCartId="
        postEndpoint += cartId
        postEndpoint += "&User="
        postEndpoint += userId
        
        let url = NSURL(string: postEndpoint)
        
        let task = NSURLSession.sharedSession().dataTaskWithURL(url!) {(data, response, error) in
            let post = JSON(data: data)
            println(post)
            for index in 0..<post.count-1
            {
                let ord:Orden = Orden(id: post[index]["ID"].string!, name: post[index]["Name"].string!)
                GlobalVariables.Ordenes.append(ord)
            }
        }
        self.performSegueWithIdentifier("DataLoaded", sender: nil)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if let destinationVC = segue.destinationViewController as? SWRevealViewController{
            //destinationVC.numberToDisplay = counter
        }
    }
}

