//
//  BaseViewContoller.swift
//  Kukimba
//
//  Created by imac on 8/7/15.
//  Copyright (c) 2015 Acc. All rights reserved.
//

import Foundation



class BaseViewController: UIViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
