//
//  CestaCompra.swift
//  Kukimba
//
//  Created by imac on 8/7/15.
//  Copyright (c) 2015 Acc. All rights reserved.
//

import Foundation

class CestaCompra : NSObject
{
    let
    Id:   Int,
    Availability:   Int,
    ItemCount:   Int,
    CestaItem: [ItemCompra]
    
    init(
        id: Int,
        availability: Int,
        itemCount: Int,
        cestaItem: [ItemCompra])
    {
        self.Id = id
        self.Availability = availability
        self.ItemCount = itemCount
        self.CestaItem = cestaItem
    }
}