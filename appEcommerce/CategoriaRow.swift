//
//  CategoriaRow.swift
//  Kukimba
//
//  Created by imac on 7/7/15.
//  Copyright (c) 2015 Acc. All rights reserved.
//

import Foundation

class CategoriaRow:NSObject
{
    var
    Principal: Categoria,
    Secundario: [Categoria]
    
    init(principal:Categoria)
    {
        self.Principal  = principal
        self.Secundario = []
    }
    
    func addCat(cat:Categoria)
    {
        //self.Secundario.insert(cat!, atIndex: cat.Position)
        self.Secundario.append(cat)
    }
}