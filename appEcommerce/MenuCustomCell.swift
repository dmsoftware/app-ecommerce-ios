//
//  CategoriasCustomCell.swift
//  Kukimba
//
//  Created by imac on 9/7/15.
//  Copyright (c) 2015 Acc. All rights reserved.
//

import UIKit

class MenuCustomCell: UITableViewCell
{
    
    @IBOutlet var menuLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setMenu(txt:String)
    {
        menuLabel.text = txt
    }

}