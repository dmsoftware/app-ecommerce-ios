//
//  PopupSeleccion.swift
//  Kukimba
//
//  Created by imac on 17/7/15.
//  Copyright (c) 2015 Acc. All rights reserved.
//

import Foundation
import UIKit

class PopupSeleccion : UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var MyTitle: UILabel!
    @IBOutlet weak var OkButton: UIButton!
    @IBOutlet weak var KoButton: UIButton!
    @IBOutlet weak var TablaOpciones: UITableView!
    
    var items: [String] = []
    var selectedIndex:Int = 0
    var Type:Int = 1
    
    var pReference:CategoriasViewContoller!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.6)
        self.popUpView.layer.cornerRadius = 5
        self.popUpView.layer.shadowOpacity = 0.8
        self.popUpView.layer.shadowOffset = CGSizeMake(0.0, 0.0)

        self.selectedIndex = GlobalVariables.subCategorie
        
        self.TablaOpciones.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    override func viewWillAppear(animated: Bool) {
        
    }
    
    func showInView(aView: UIView!, withMessage message: String!, elements: [String], p:CategoriasViewContoller, type:Int, animated: Bool)
    {
        items = elements
        self.pReference = p
        selectedIndex = -1
        self.Type = type
        aView.addSubview(self.view)
        MyTitle.text = message
        /*logoImg!.image = image
        messageLabel!.text = message*/
        if animated
        {
            self.showAnimate()
        }
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
        self.view.alpha = 0.0;
        UIView.animateWithDuration(0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransformMakeScale(1.0, 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animateWithDuration(0.25, animations: {
            self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
        });
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell:UITableViewCell = self.TablaOpciones.dequeueReusableCellWithIdentifier("cell") as! UITableViewCell
        cell.textLabel?.text = self.items[indexPath.row]
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        self.selectedIndex = indexPath.row
    }
    
    @IBAction func OkClicked(sender: UIButton) {
        if(self.selectedIndex > -1)
        {
            if(self.Type == 1){
                self.pReference.acceptSubCategoria(self.selectedIndex, catS: self.items[self.selectedIndex])
            }else{
                self.pReference.acceptOrden(self.selectedIndex)
            }
        }
        self.removeAnimate()
    }
    
    @IBAction func KoClicked(sender: UIButton) {
        self.removeAnimate()
    }
    
    
}