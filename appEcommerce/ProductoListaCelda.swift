//
//  ProductoListaCelda.swift
//  Kukimba
//
//  Created by imac on 13/7/15.
//  Copyright (c) 2015 Acc. All rights reserved.
//

import Foundation

class ProductoListaCelda : UIViewController
{
    
    @IBOutlet weak var Imagen: UIImageView!
    
    @IBOutlet weak var MarcaLbl: UILabel!
    @IBOutlet weak var ProductoLbl: UILabel!
    @IBOutlet weak var PrecioLbl: UILabel!
    @IBOutlet weak var PrecioOriginalLbl: UILabel!
    
    @IBOutlet weak var RebajaImg: UIImageView!
    @IBOutlet weak var RebajaLbl: UILabel!
    
    @IBOutlet weak var reloj: UIActivityIndicatorView!
    
    func inicializar(marca:String, producto:String, precio:Int, precioOrig:Int, imgURL:String, fuenteS:CGFloat)
    {
        MarcaLbl.font = MarcaLbl.font.fontWithSize(fuenteS)
        MarcaLbl.text = marca
        ProductoLbl.font = ProductoLbl.font.fontWithSize(fuenteS)
        ProductoLbl.text = producto
        PrecioLbl.font = PrecioLbl.font.fontWithSize(fuenteS)
        PrecioLbl.text = String(precio) + " €"
        PrecioOriginalLbl.font = PrecioOriginalLbl.font.fontWithSize(fuenteS)
        PrecioOriginalLbl.text = String(precioOrig) + " €"
        
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(precioOrig) + " €")
        attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 1, range: NSMakeRange(0, attributeString.length))
        
        PrecioOriginalLbl.attributedText = attributeString
        
        let urlString = "http://images.kukimba.com/item/233x155/" + imgURL
        
        loadImageAsync(urlString)
    }
    
    func loadImageAsync(urlString:String)
    {
        let imgURL = NSURL(string: urlString)
        let request: NSURLRequest = NSURLRequest(URL: imgURL!)
        let mainQueue = NSOperationQueue.mainQueue()
        NSURLConnection.sendAsynchronousRequest(request, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
            if error == nil {
                // Convert the downloaded data in to a UIImage object
                let image = UIImage(data: data)
                dispatch_async(dispatch_get_main_queue(), {
                     self.Imagen?.image = image
                     //self.Imagen?.layer.borderColor = UIColor.blackColor().CGColor
                     //self.Imagen?.layer.borderWidth = 1.0
                     self.reloj.hidden = true
                })
            }
            else {
                println("Error: \(error.localizedDescription)")
            }
        })
    }
    
}