//
//  Categoria.swift
//  Kukimba
//
//  Created by imac on 7/7/15.
//  Copyright (c) 2015 Acc. All rights reserved.
//

import Foundation

/**
Data entity that represents a geographic area.
Subclasses NSObject to enable Obj-C instantiation.
*/
class Categoria : NSObject
{
    let
    ID:   Int,
    MerchantID:  String,
    Name:       String,
    Parent: Int,
    Position: Int
    
    init(
        id: Int,
        merchantid: String,
        name: String,
        parent: Int,
        position: Int)
    {
        self.ID = id
        self.MerchantID = merchantid
        self.Name = name
        self.Parent = parent
        self.Position = position
    }
}