//
//  GlobalVariables.swift
//  Kukimba
//
//  Created by imac on 9/7/15.
//  Copyright (c) 2015 Acc. All rights reserved.
//

import Foundation

struct GlobalVariables
{
    static var systemVersion:Double!
    
    static var Categorias:[CategoriaRow] = []
    static var Ordenes:[Orden] = []
    
    static var mainCategorie:Int = 0
    static var subCategorie:Int = 0
    static var momentOrder:Int = 0
    static var productID:Int = 0
}