//
//  CategoriasViewContoller.swift
//  Kukimba
//
//  Created by imac on 9/7/15.
//  Copyright (c) 2015 Acc. All rights reserved.
//

import Foundation
import UIKit

class CategoriasViewContoller: UIViewController, UIScrollViewDelegate
{
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var ListaScroller: UIScrollView!
    @IBOutlet weak var ListaContainer: UIView!
    
    @IBOutlet weak var catButton: UIButton!
    @IBOutlet weak var ordenButton: UIButton!
    @IBOutlet weak var filButton: UIButton!
    
    var popViewCategorias : PopupSeleccion!
    var popViewFiltros : PopupFiltros!
    
    var maxRes:Int = 19
    var currPage:Int = 1
    var MerchID:String = ""
    var Order:String = "asc"
    var Filter:String = ""
    var altura:CGFloat = 4
    
    var positionYScroller:CGFloat = 0
    
    var chargingData:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            // Uncomment to change the width of menu
            //self.revealViewController().rearViewRevealWidth = 62
        }
        
        self.title = GlobalVariables.Categorias[GlobalVariables.mainCategorie].Principal.Name
        
        self.catButton.setTitle(GlobalVariables.Categorias[GlobalVariables.mainCategorie].Secundario[GlobalVariables.subCategorie].Name, forState: .Normal)
    }
    
    override func viewWillAppear(animated: Bool) {
        fetchProductos()
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            //reach bottom
            //println("Bottom")
            if(chargingData==false)
            {
                chargingData = true
                currPage++
                self.positionYScroller = self.ListaScroller.contentOffset.y
                fetchProductos()
            }
        }
        /*if (scrollView.contentOffset.y < 0){
            //reach top
            println("Top")
        }
        if (scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y < (scrollView.contentSize.height - scrollView.frame.size.height)){
            //not top and not bottom
            println("Not Top, Not Bottom")
        }*/
    }
    
    private func fetchProductos()
    {
        MerchID = GlobalVariables.Categorias[GlobalVariables.mainCategorie].Principal.MerchantID
        Filter = GlobalVariables.Categorias[GlobalVariables.mainCategorie].Secundario[GlobalVariables.subCategorie].MerchantID
        
        var postEndpoint: String = "http://www.kukimba.com/_app-api/service-api.aspx?method=GetProductList"
        
        postEndpoint += "&maxResults="
        postEndpoint += String(maxRes)
        postEndpoint += "&currentPage="
        postEndpoint += String(currPage)
        postEndpoint += "&categories="
        postEndpoint += MerchID
        postEndpoint += "&order="
        postEndpoint += Order
        postEndpoint += "&filters="
        postEndpoint += Filter
        
        println(postEndpoint)
        
        let url = NSURL(string: postEndpoint)
        
        //let task = NSURLSession.sharedSession().dataTaskWithURL(url!) {(data, response, error) in
        if let data = NSData(contentsOfURL: url!, options: .allZeros, error: nil) {
            //println(NSString(data: data, encoding: NSUTF8StringEncoding))
            let post = JSON(data: data)
            // now we have the results, let's just print them though a tableview would definitely be better UI:
            //println(post)
            var screeW:CGFloat = UIScreen.mainScreen().bounds.width
            //var sRel:CGFloat = UIScreen.mainScreen().bounds.height / UIScreen.mainScreen().bounds.width
            var sRel:CGFloat = 1
            if(post.count>0)
            {
                for index in 0..<post.count-1
                {
                    var cellProduct:ProductoListaCelda = ProductoListaCelda(nibName:"ProductoListaCelda", bundle: nil)
                
                    self.addChildViewController(cellProduct)
                    self.ListaContainer.addSubview(cellProduct.view)
                    cellProduct.didMoveToParentViewController(self)
                    //cellProduct.delegate = self
                
                    let lagMod:CGFloat = CGFloat(index % 2)
                
                    var lagx = (screeW/2-2) * lagMod + 4
                    if((index>0)&&(index%2 == 0))
                    {
                        altura = altura + ((screeW-12)/2 * sRel+4)
                    }
                    var lagw = (screeW-12)/2
                    var lagh = (screeW-12)/2*sRel
                
                    cellProduct.view.frame = CGRect(x:CGFloat(lagx), y:CGFloat(altura), width:CGFloat(lagw), height:CGFloat(lagh))
                
                    cellProduct.inicializar(post[index]["Brand"].string!, producto: post[index]["Name"].string!, precio: post[index]["Price"].int!, precioOrig: post[index]["OriginalPrice"].int!, imgURL: post[index]["Image"][0].string!, fuenteS: lagw/20)
                
                }
                chargingData = false
            }
            altura = altura + ((screeW-12)/2 * sRel+4)
            let contSize = CGSizeMake(screeW, altura)        
            self.ListaScroller.contentSize = contSize
            self.ListaScroller.contentOffset = CGPoint(x: 0, y: 0)
            self.ListaScroller.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            if(GlobalVariables.systemVersion<8.0)
            {
                self.ListaContainer.frame = CGRect(x:CGFloat(0), y:CGFloat(0), width:CGFloat(self.view.frame.width), height:altura)
            }
            self.ListaScroller.contentOffset.y = self.positionYScroller
        }
    }
    
    @IBAction func CategoriaClicked(sender: UIButton) {
        self.popViewCategorias = PopupSeleccion(nibName: "PopupSeleccion", bundle: nil)
        self.popViewCategorias.view.frame = CGRect(x:0, y:0, width:UIScreen.mainScreen().bounds.width, height:UIScreen.mainScreen().bounds.height)
        
        var items: [String] = []
        
        for cat:Categoria in GlobalVariables.Categorias[GlobalVariables.mainCategorie].Secundario
        {
            items.append(cat.Name)
        }
        
        self.popViewCategorias.showInView(self.view, withMessage: "Categorias", elements: items, p:self, type:1, animated: true)
        
        /*var screeW:CGFloat = UIScreen.mainScreen().bounds.width - 100
        var screeH:CGFloat = UIScreen.mainScreen().bounds.height - 100
        
        popupCat.view.frame = CGRect(x:50, y:50, width:screeW, height:screeH)*/
        
        /*cellProduct.inicializar(post[index]["Brand"].string!, producto: post[index]["Name"].string!, precio: post[index]["Price"].int!, precioOrig: post[index]["OriginalPrice"].int!, imgURL: post[index]["Image"][0].string!, altura: lagh/5)*/
    }
    
    @IBAction func OrdenClicked(sender: UIButton) {
        self.popViewCategorias = PopupSeleccion(nibName: "PopupSeleccion", bundle: nil)
        self.popViewCategorias.view.frame = CGRect(x:0, y:0, width:UIScreen.mainScreen().bounds.width, height:UIScreen.mainScreen().bounds.height)
        
        var items: [String] = []
        
        for ord:Orden in GlobalVariables.Ordenes
        {
            items.append(ord.Name)
        }
        
        self.popViewCategorias.showInView(self.view, withMessage: "Orden", elements: items, p:self, type:2, animated: true)

    }
   
    @IBAction func FiltrosClicked(sender: UIButton) {
        self.popViewFiltros = PopupFiltros(nibName: "PopupFiltros", bundle: nil)
        self.popViewFiltros.view.frame = CGRect(x:0, y:0, width:UIScreen.mainScreen().bounds.width, height:UIScreen.mainScreen().bounds.height)
        
        var items: [String] = []
        
        for ord:Orden in GlobalVariables.Ordenes
        {
            items.append(ord.Name)
        }
        
        self.popViewCategorias.showInView(self.view, withMessage: "Filtros", elements: items, p:self, type:2, animated: true)
    }
    
    func acceptSubCategoria(cat:Int,catS:String)
    {
        GlobalVariables.subCategorie = cat
        self.catButton.setTitle(catS, forState: .Normal)
        altura = 4
        currPage = 1
        for view in self.ListaContainer.subviews {
            view.removeFromSuperview()
        }
        self.ListaScroller.contentOffset = CGPoint(x: 0, y: 0)
        self.fetchProductos()
    }
    
    func acceptOrden(orden:Int)
    {
        self.Order = GlobalVariables.Ordenes[orden].ID
        altura = 4
        currPage = 1
        for view in self.ListaContainer.subviews {
            view.removeFromSuperview()
        }
        self.ListaScroller.contentOffset = CGPoint(x: 0, y: 0)
        self.fetchProductos()

    }

    func acceptFilter(orden:Int)
    {
        self.Order = GlobalVariables.Ordenes[orden].ID
        altura = 4
        currPage = 1
        for view in self.ListaContainer.subviews {
            view.removeFromSuperview()
        }
        self.ListaScroller.contentOffset = CGPoint(x: 0, y: 0)
        self.fetchProductos()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if(GlobalVariables.systemVersion<8.0)
        {
            self.ListaScroller.contentSize = CGSize(width:UIScreen.mainScreen().bounds.width, height:altura)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        return CGSizeMake((UIScreen.mainScreen().bounds.width-15)/2,(UIScreen.mainScreen().bounds.width-15)/1.5); //use height whatever you wants.
    }*/
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}