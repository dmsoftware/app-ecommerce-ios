IOS gailu mugikorretako, produktu salmentarako merkataritza elektronikoko aplikazioa.
Aplikazioa merkataritza elektronikoaren web zerbitzuekin bateratzen da eta komertzio bakoitzarentzat moldatu beharko da.

SOFTWAREA HAU INONGO BERMERIK GABE ZABALTZEN DA. ESKARITARAKO EDOZEIN GERTAEREN AURREAN , KALTEAK EDO BESTELAKOAK, EGILEAK EDO COPYRIGHT-EUSLEAK EZ DIRA ERANTZULE IZANGO.